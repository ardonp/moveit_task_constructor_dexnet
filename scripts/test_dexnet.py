# ####!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
# import from the rosnode the service args
from moveit_task_constructor_dexnet.srv import GQCNNGrasp, GQCNNGraspRequest, GQCNNGraspResponse

if __name__ == "__main__":
    rospy.wait_for_service('gqcnn_grasp', 5.0)

    # creating the service handler
    dexnet_grasp_srv = rospy.ServiceProxy('gqcnn_grasp', GQCNNGrasp)

    # opening a service request
    req = GQCNNGraspRequest()
    print(req)
    # req.color_img_file_path = '/home/pardon/ws_grasp/src/deep_grasp_demo/moveit_task_constructor_dexnet/data/images/rgb_berry.png'
    # req.depth_img_file_path = '/home/pardon/ws_grasp/src/deep_grasp_demo/moveit_task_constructor_dexnet/data/images/depth_berry.png'

    res = dexnet_grasp_srv(req)
    print(res)

    # print(dexnet_grasp_solutions)
