#!/usr/bin/env python
import rospy
import rospkg
from visualization_msgs.msg import Marker
from geometry_msgs.msg import PoseStamped, PoseArray
from tf.transformations import quaternion_from_euler

import tf
import tf_conversions
import pickle

import numpy as np
import math

mesh_pub = rospy.Publisher('grasp_mesh', Marker, queue_size=10)

def plot_logo():

	marker = Marker()
	marker.header.frame_id = 'panda_link0'
	marker.ns = "rotated"
	marker.type = marker.MESH_RESOURCE
	marker.id = 1
	marker.mesh_resource = "package://graspit_demo/models/mesh/whole_logo.dae"
	marker.action = marker.ADD
	marker.scale.x = 0.25
	marker.scale.y = 0.25
	marker.scale.z = 0.25
	marker.pose.position.x = 0.5
	marker.pose.position.y = 5
	marker.pose.position.z = 0.5
	marker.pose.orientation.x = 0
	marker.pose.orientation.y = 0
	marker.pose.orientation.z = 0
	marker.pose.orientation.w = 0
	marker.mesh_use_embedded_materials = True
	mesh_pub.publish(marker)

	return marker

if __name__ == "__main__":
#
	rospy.init_node("logo")
	r = rospy.Rate(1)
	while (not rospy.is_shutdown()):
		r.sleep()

		plot_logo()
