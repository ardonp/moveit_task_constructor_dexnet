# Dex-Net Setup
Demo showing how to use Dex-Net for grasp pose generation within the MoveIt Task Constructor. This code is adapted from the [MoveIt Task Constructor](https://github.com/PickNikRobotics/deep_grasp_demo/tree/master/moveit_task_constructor_dexnet). Dex-Net uses a grasp quality CNN (GQ-CNN) to compose the probability a grasp candidate will be successful. The inputs are a color and a depth image and outputs a list of the grasp candidates and their probabilities of success.

The process of sampling grasps using Dex-Net is messy because the library is written in python 2 which is not officially supported in ROS Melodic. The `grasp_image_detection` node calls the `gqcnn_grasp` service offered by the `gqcnn_server` node which returns the grasp candidates. The `gqcnn_server` node interacts with a python 3 `grasp_detector` script running the Dex-Net 4.0 policy. The images are saved by the `process_image_server` node and the files are then loaded by `grasp_detector` script.

## Installing
- If you are using ROS 1 then you need to set a virtual environment for python 3 (used by DexNet). Create the environment using this useful [link](https://www.linode.com/docs/guides/create-a-python-virtualenv-on-ubuntu-18-04/).

- Run the install script to download the requirements
If you have a GPU this option will install tensorflow with GPU support. This script will install packages for Python 3.
```
cd ~/ws_grasp_baselines/src
git clone https://ardonpaola@bitbucket.org/ardonp/moveit_task_constructor_dexnet.git
wget https://raw.githubusercontent.com/PickNikRobotics/deep_grasp_demo/master/dexnet_install.sh
wget https://raw.githubusercontent.com/PickNikRobotics/deep_grasp_demo/master/dexnet_requirements.txt
chmod +x dexnet_install.sh
./dexnet_install.sh {cpu|gpu}
```

- Download the pretrained models
```
./dexnet_deps/gqcnn/scripts/downloads/models/download_models.sh
```

- Set the configuration file paths

In `moveit_task_constructor_dexnet/config/dexnet_config.yaml` specify the absolute file paths to the `model_dir` and `model_params` parameters for the Dex-Net 4.0 parallel jaw configuration. The `model_name` is already set to use the Dex-Net 4.0 parallel jaw configuration. The `model_dir` parameter specifies the path to the learned model weights located in `gqcnn/cfg/examples/replication/dex-net_4.0_pj.yaml` and the` model_params` parameter specifies the model configuration located in `gqcnn/models`. If you use the `dexnet_install.sh` script the `gqcnn` directory will be located inside the `dexnet_deps` directory.

- Install ROS package
```
cd ~/ws_grasp_baselines/
catkin build
echo 'source ~/ws_grasp_baselines/devel/setup.bash' >> ~/.bashrc
```

## Running
### Using Fake Controllers
You don't need Gazebo for this one. The images for the cylinder were collected ahead of time and located in `data/images/depth_cylinder.png` and `data/images/rgb_cylinder.png`.

To run the pick and place demo:
```
roslaunch moveit_task_constructor_dexnet dexnet_demo.launch
```
- To see the generated grasps with their cost (generated at `~/moveit_task_constructor_dexnet/src/grasp_detection.cpp` and remember to have a `roscore` open in another terminal)
```
rostopic echo /generate_grasps/feedback
```

**Important Note:** It is recommended to use the provided images for the cylinder demo. The Dex-Net data set contains only images using a downward facing camera, therefore, the GQ-CNN works best using images taken from above. The provided images of the cylinder have been tested and work with this demo. If you use set `load_images:=false` the demo is less likely to work. However, if you change the camera position to overhead as described in the `Camera View Point` section and use a different object such as the bar clamp then Dex-Net will perform better when `load_images:=false`.

To run the pick and place demo:
```
roslaunch moveit_task_constructor_dexnet dexnet_demo.launch
```


## Nodes
### grasp_image_detection
This node bridges the gap between Dex-Net and the MoveIt Task Constructor. Communication with the MoveIt Task Constructor is achieved using ROS action messages. The action client sends the grasp candidates along with the costs back to the `DeepGraspPose` stage. When an action goal is received the `gqcnn_grasp` service is called and the grasp candidates from Dex-Net are received. The images can either be loaded from a file or the node will call the `save_images` service to collect the data.

### gqcnn_server
This node interacts with a python 3 `grasp_detector` script running the Dex-Net 4.0 policy. The `grasp_detector` script is executed as a subprocess to the node and communication with the node is achieved through a Unix Pipe. This node offers a `gqcnn_grasp` service that returns the sampled grasps and their probabilities of success.

### process_image_server
This node is used primarily to save images. It subscribes to color and depth image topic names of type `sensor_msgs/Image`. It offers a `save_images` service that will save the color and depth images to the user specified file names.


## Tips
Adjust some of the parameters in `gqcnn/cfg/examples/replication/dex-net_4.0_pj.yaml`. The `num_seed_samples` parameter determines how many grasps are sampled from the depth image. The `deterministic` parameter will deterministically sample the grasp candidates. Set this to 0 to get different results.


